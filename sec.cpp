#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <sstream>

using namespace std;

int main()
{
    int n;
    
    cin >> n;
    vector<int> old(n);
    vector<int> nv(n);
    
    for (int i = 0; i < n; ++i)
    {
        cin >> old[i];
        nv[i] = old[i];
    }
    
    sort(nv.begin(), nv.end());
    
    int diff_count = 0;
    for (int i = 0; i < n; ++i)
    {
        if (nv[i] != old[i])
        {
            ++diff_count;
        }
    }

    if (diff_count == 0)
    {
       cout << diff_count << endl;
    }
    else
    {
	int it_count = 0;
        ostringstream solve;

        for (int i = 0; i < n; ++i)
        {
            if (nv[i] != old[i])
            {
                int pos = 0;

                for (; pos < n; ++pos)
                {
                    if (old[pos] == nv[i] && old[pos] != nv[pos])
                    {
                        solve << pos << " " << i << endl;
			swap(old[pos], old[i]);
			++it_count;
			break;
                    }
                }
            }
        }

        cout << it_count << endl << solve.str();
    }
}
