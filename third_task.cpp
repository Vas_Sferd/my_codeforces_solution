#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int max_num(int len, int sum)
{
    if (sum <= len * 9 && (sum >= len || (sum == 0 && len == 1)))
    {
        int result = 0;

        while (len > 0)
        {
            int a = min(sum, 9);
            sum -= a;

            result = (result + a) * 10;
            --len;
        }

        result /= 10;
        return result;
    }
    else
    {
        return -1;
    }
}

int min_num(int len, int sum)
{
    if (sum <= len * 9 && (sum >= len || (sum == 0 && len == 1)))
    {
        int result = 1 * pow(10, len - 1);
        --sum;

        for (int i = 0; sum > 0; ++i)
        {
            int a = min(sum, 9);
            sum -= a;

            result = result + a * pow(10, i);
        }

        return result;
    }
    else
    {
        return -1;
    }
}

int main()
{
    int len, sum;
    cin >> len >> sum;

    cout << min_num(len, sum) << " " << max_num(len, sum);
}
